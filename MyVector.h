#pragma once
#include <iostream>

// @see https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c8-use-class-rather-than-struct-if-any-member-is-non-public
// but @see https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c2-use-class-if-the-class-has-an-invariant-use-struct-if-the-data-members-can-vary-independently
class MyVector
{
// @see http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#nl16-use-a-conventional-class-member-declaration-order
public: 
	MyVector(int x, int y, int z) : x(x), y(y), z(z) { }
	int GetX() { return x; };
	int GetY() { return y; };
	int GetZ() { return z; };
	double GetLength();
	// @see https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c167-use-an-operator-for-an-operation-with-its-conventional-meaning
	friend std::ostream& operator<<(std::ostream& stream, const MyVector& value);

// @see https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rc-private
private: 
	int x = 0;
	int y = 0;
	int z = 0;
};