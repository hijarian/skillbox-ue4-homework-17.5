#include <iostream>
#include "MyVector.h"

int main()
{
	MyVector vector{ 1, 2, 3 };

	std::cout << "Length of the vector" << vector << " is " << vector.GetLength() << std::endl;
}