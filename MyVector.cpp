#include "MyVector.h"
#include <cmath>

double MyVector::GetLength()
{
	return std::hypot(static_cast<double>(this->x), static_cast<double>(this->y), static_cast<double>(this->z));
};

std::ostream& operator<<(std::ostream& stream, const MyVector& value)
{
	stream << "(" << value.x << ", " << value.y << ", " << value.z << ")";
	return stream;
}
